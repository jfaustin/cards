# Card Game Challenge Requirement

This is the classic implementation of the card game of War. The requirements were sent as follows:

Write the logic for a game of cards.

The game has 2 players. A deck of cards has a total of 52 cards with 13 cards in each suite (Hearts, Spade, Clubs and Diamonds). J, Q, K and A have a numerical value of 11. The value of the rest of the cards is their face value.

Deal the cards between the 2 players.

Player 1 then drops a card in the order in which it was dealt to him/her. Player 2 drops a card on top of Player 1's card in the order in which it was dealt to him/her. If Player 1's card has a higher numerical value Player 1 gets 1 point. If Player 2's card has a higher numerical value Player 2 gets 1 point.

If the card dropped by both players is same, the round is tied and the player who wins the next round gets a point for the tied round as well. At the end of the game, declare the winner.

Please print the cards dealt to each player in the order in which they were dealt.

# Running this app

Built on Node 9.3.0.

* Check out the code
* run `node src/index.js` or `npm start`

# Questions and Assumptions

If I was doing this in a real environment, there would be clarification from the product standpoint that would need to happen. As this is just a test, the following assumptions were made.

## How will someone interact with this system?
The assumption is that a user will have access to the command line and the output will be sent to stdout

## Will there ever be more than just 2 players?
The assumption is that only 2 players will be allowed to play the game of War

## Should there be a tie breaker if all parties collect the same number of points
The assumption is that the game can end in a tie. I would ask the stakeholders about using tiebreakers such as rounds won, total points for high cards, etc.

## If multiple ties happen in a row, do tie breaker points add up?
The assumption is that they do, just as the game of War works.

## What should happen to the points if the final round ends in a tie?
The points are never assigned

# Testing this app

I wrote a few tests in mocha just to show a proof of concept. Run the tests by executing `node test` after `npm install` to install dependencies.
