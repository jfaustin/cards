const expect = require("chai").expect;
const War = require("../src/games/War/War");
const DeckOfCards = require("../src/lib/classes/DeckOfCards");
const Player = require("../src/lib/classes/Player");
const Card = require("../src/lib/classes/Card");

describe("War Card Tests", function() {
    describe("Constructor Test", function() {

        it("Face cards value 11", function() {
            let jack = new Card(1, 11);
            let queen = new Card(1, 12);
            let king = new Card(1, 13);
            let ace = new Card(1, 14);

            expect(jack.value).to.equal(11) &&
            expect(queen.value).to.equal(11) &&
            expect(king.value).to.equal(11) &&
            expect(ace.value).to.equal(11);
        });
    });
});