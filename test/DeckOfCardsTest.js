const expect = require("chai").expect;
const War = require("../src/games/War/War");
const DeckOfCards = require("../src/lib/classes/DeckOfCards");
const Player = require("../src/lib/classes/Player");
const Card = require("../src/lib/classes/Card");

describe("Deck of Cards Tests", function() {
    describe("Constructor Test", function() {
        it("26 cards each", function() {
            let deck = new DeckOfCards();

            let player1 = new Player("Joannie");
            let player2 = new Player("Chachi");

            let game = new War(deck, player1, player2);

            let results = game.play();

            expect(results.player1.hand.cards.length).to.equal(26) && expect(results.player2.hand.cards.length).to.equal(26);
        });

        it("13 cards of each suite", function() {
            let hearts = 0;
            let clubs = 0;
            let diamonds = 0;
            let spades = 0;

            let deck = new DeckOfCards();
            for (var i = 0; i < deck.cards.length; i++) {
                switch (deck.cards[i].suit) {
                    case 1:
                        diamonds++;
                        break;
                    case 2:
                        spades++;
                        break;
                    case 3:
                        hearts++;
                        break;
                    case 4:
                        clubs++;
                        break;
                }
            }

            expect(diamonds).to.equal(13) &&
            expect(spades).to.equal(13) &&
            expect(clubs).to.equal(13) &&
            expect(hearts).to.equal(13);
        });
    });
});