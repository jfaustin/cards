const expect = require("chai").expect;
const War = require("../src/games/War/War");
const DeckOfCards = require("../src/lib/classes/DeckOfCards");
const Player = require("../src/lib/classes/Player");
const Card = require("../src/lib/classes/Card");

describe("War Game Tests", function() {
    describe("Play Test", function() {
        it("Player 1 wins", function() {
            let deck = new DeckOfCards();
            deck.cards = [
                new Card(1, 9),
                new Card(1, 6)
            ];

            let player1 = new Player("Joannie");
            let player2 = new Player("Chachi");

            let game = new War(deck, player1, player2);

            let results = game.play();

            expect(results.rounds[0].winner.id).to.equal(player1.id);
        });

        it("Player 2 wins", function() {
            let deck = new DeckOfCards();
            deck.cards = [
                new Card(1, 9),
                new Card(1, 13)
            ];

            let player1 = new Player("Joannie");
            let player2 = new Player("Chachi");

            let game = new War(deck, player1, player2);

            let results = game.play();

            expect(results.rounds[0].winner.id).to.equal(player2.id);
        });

        it("Tie", function() {
            let deck = new DeckOfCards();
            deck.cards = [
                new Card(1, 9),
                new Card(2, 9)
            ];

            let player1 = new Player("Joannie");
            let player2 = new Player("Chachi");

            let game = new War(deck, player1, player2);

            let results = game.play();

            expect(results.rounds[0].winner).to.equal(null);
        });

        it("Tie with Different Face Cards", function() {
            let deck = new DeckOfCards();
            deck.cards = [
                new Card(1, 13),
                new Card(2, 11)
            ];

            let player1 = new Player("Joannie");
            let player2 = new Player("Chachi");

            let game = new War(deck, player1, player2);

            let results = game.play();

            expect(results.rounds[0].winner).to.equal(null);
        });

        it("Bonus Points", function() {
            let deck = new DeckOfCards();
            deck.cards = [
                new Card(1, 13),
                new Card(2, 11),
                new Card(1, 9),
                new Card(2, 3)
            ];

            let player1 = new Player("Joannie");
            let player2 = new Player("Chachi");

            let game = new War(deck, player1, player2);

            let results = game.play();

            expect(results.rounds[1].bonusPointsAwarded).to.equal(1);
        });
    });
});