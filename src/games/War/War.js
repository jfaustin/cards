const Game = require("../../lib/classes/Game");
const WarGameResult = require("./WarGameResult");
const WarRoundResult = require("./WarRoundResult");
const pointsWonPerRound = 1;

/**
 * Ipmlements the classic card game of war, which includes 2 players. Each player is dealt an alternating card from
 * a deck until the deck has no more cards.
 *
 * A round consists of each player blindly playing a card in the order that the card was dealt. The player with the
 * highest face value wins the hand and a point. If both cards have the same face value, no points are awarded that round
 * but the winner of the next round receives the points as a bonus.
 *
 * The player with the most points at the end of the game wins!
 */
class War extends Game {

    /**
     *
     * @param DeckOfCards deck
     * @param Player player1
     * @param Player player2
     */
    constructor(deck, player1, player2) {
        super(deck);
        this.addPlayer(player1);
        this.addPlayer(player2);
    }

    /**
     * play the game of war
     *
     * @returns WarGameResult
     */
    play() {
        if (this.players.length != 2) {
            throw new Error("War must be played between 2 players and ONLY 2 players!");
        }

        // lets deal
        let player1 = this.players[0];
        let player2 = this.players[1];

        let results = new WarGameResult(player1, player2);

        for (var cardDealt = 0; cardDealt < this.deck.cards.length; cardDealt++) {
            if (cardDealt % 2 == 0) {
                player1.hand.add(this.deck.cards[cardDealt]);
            } else {
                player2.hand.add(this.deck.cards[cardDealt]);
            }
        }

        let player1Card = null;
        let player2Card = null;
        let tieStreak = 0;

        // 52 cards, 2 players, so 26 rounds of war
        for (var round = 0; round < this.deck.cards.length / 2; round++) {

            player1Card = player1.hand.cards[round];
            player2Card = player2.hand.cards[round];
            
            var roundResult = new WarRoundResult(round + 1);
            roundResult.player1Card = player1Card;
            roundResult.player2Card = player2Card;

            if (player1Card.value == player2Card.value) {
                tieStreak++;
                results.addRound(roundResult);
                continue;
            }

            if (player1Card.value > player2Card.value) {
                roundResult.winner = player1;
            } else {
                roundResult.winner = player2;
            }

            roundResult.pointsAwarded = pointsWonPerRound;
            roundResult.bonusPointsAwarded = tieStreak * pointsWonPerRound;
            results.addRound(roundResult);

            // reset tie streak because someone won
            tieStreak = 0;
        }

        return results;
    }
}

module.exports = War;