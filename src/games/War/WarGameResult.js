var Player = require("../../lib/classes/Player");

/**
 * Result from a full game of War
 */
class WarGameResult {

    /**
     *
     * @param Player player1
     * @param Player player2
     */
    constructor(player1, player2) {
        if (!(player1 instanceof Player)) {
            throw new Error("Player 1: Only players can be added to the game!");
        }

        if (!(player2 instanceof Player)) {
            throw new Error("Player 2: Only players can be added to the game!");
        }

        this.player1 = player1;
        this.player2 = player2;
        this.rounds = [];

        this.player1Points = 0;
        this.player2Points = 0;
    }

    /**
     *
     * @param WarRoundResult roundResult
     */
    addRound(roundResult) {
        if (roundResult.winner != null) {
            if (roundResult.winner.id == this.player1.id) {
                this.player1Points += roundResult.pointsAwarded;
            } else if (roundResult.winner.id == this.player2.id) {
                this.player2Points += roundResult.pointsAwarded;
            }
        }

        this.rounds.push(roundResult);
    }
}

module.exports = WarGameResult;