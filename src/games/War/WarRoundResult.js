/**
 * Result of a round of War
 */
class WarRoundResult {

    /**
     *
     * @param int number - What round number was played
     */
    constructor(number) {
        this.number = number;
        this.player1Card = null;
        this.player2Card = null;
        this.winner = null;
        this.pointsAwarded = 0;
        this.bonusPointsAwarded = 0;
    }
}

module.exports = WarRoundResult;