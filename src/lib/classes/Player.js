const CardCollection = require("./CardCollection");

/**
 * Players have IDs that uniquely identify them, a name, and a hand of cards
 */
class Player {

    /**
     *
     * @param string name
     */
    constructor (name) {
        this.id = Math.floor(Math.random() * Math.floor(10000));
        this.name = name;
        this.hand = new CardCollection();
    }
}

module.exports = Player;