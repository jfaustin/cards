const Card = require("./Card");

/**
 * Deck of cards. This deck assumes no jokers, so 52 total cards, 13 from each suite.
 */
class DeckOfCards {

    /**
     * generates all the cards in the deck
     */
    constructor() {
        this.cards = [];

        for (var suit = 1; suit <= 4; suit++) {
            for (var face = 2; face <= 14; face++) {
                this.cards.push(new Card(suit, face));
            }
        }
    }

    /**
     * shuffles the cards
     */
    shuffle() {
        let counter = this.cards.length;

        while (counter > 0) {
            let index = Math.floor(Math.random() * counter);
            counter--;

            let temp = this.cards[counter];
            this.cards[counter] = this.cards[index];
            this.cards[index] = temp;
        }
    }
}

module.exports = DeckOfCards;