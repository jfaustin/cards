const Card = require("./Card");

/**
 * A card collection is any collection of Card objects
 */
class CardCollection {

    constructor() {
        this.cards = [];
    }

    add(card) {
        if (!(card instanceof Card)) {
            throw new Error("Only cards can be added to the deck!");
        }

        this.cards.push(card);
    }
}

module.exports = CardCollection;