const DIAMOND = 1;
const SPADE = 2;
const HEART = 3;
const CLUB = 4;

/**
 * Represents a playing card of one of the 4 suites. All face cards carry the same value of 11.
 *
 */
class Card {

    /**
     *
     * @param int suit
     * @param int face
     */
    constructor (suit, face) {
        if (suit < 1 || suit > 4) {
            throw new Error("Not a recognized suit");
        }

        if (face < 1 || face > 14) {
            throw new Error("Not a recognized card");
        }

        this.suit = suit;
        this.face = face;
        this.value = (face > 10) ? 11 : face;
    }

    /**
     *
     * @returns string
     */
    toString() {
        let ret = "";

        if (this.face <= 10) {
            ret = this.face;
        } else if (this.face == 11) {
            ret = "Jack";
        } else if (this.face == 12) {
            ret = "Queen";
        } else if (this.face == 13) {
            ret = "King";
        } else if (this.face == 14) {
            ret = "Ace";
        }

        ret += " of ";

        switch (this.suit) {
        case DIAMOND:
            ret += "Diamonds";
            break;
        case SPADE:
            ret += "Spades";
            break;
        case HEART:
            ret += "Hearts";
            break;
        case CLUB:
            ret += "Clubs";
            break;
        }

        return ret;
    }
}

module.exports = Card;