var Player = require("./Player");
var DeckOfCards = require("./DeckOfCards");

/**
 * Any card game has a deck and players. The play() method should be implemented. This is a mock abstract class.
 */
class Game
{
    /**
     *
     * @param DeckOfCards deck
     */
    constructor(deck) {
        if (!(deck instanceof DeckOfCards)) {
            throw new Error("Include a DeckOfCards deck to get started.");
        }

        this.deck = deck;
        this.players = [];
    }

    /**
     *
     * @param Player player
     */
    addPlayer(player) {
        if (!(player instanceof Player)) {
            throw new Error("Only players can be added to the game!");
        }

        this.players.push(player);
    }

    /**
     * Abstract method must be implemented by whatever extends this class
     */
    play() {
        throw new Error("You must implement this method!");
    }
}

module.exports = Game;