/**
 * Plays the game of war with 2 players.
 *
 */

"use strict";
const DeckOfCards = require("./lib/classes/DeckOfCards");
const Player = require("./lib/classes/Player");

const War = require("./games/War/War");

let player1 = new Player("Joannie");
let player2 = new Player("Chachi");

let cards = new DeckOfCards();
cards.shuffle();

let game = new War(cards, player1, player2);

let results = game.play();

for (var round = 0; round < results.rounds.length; round++) {

    console.log("\n-------------------------------\n");
    console.log("Round " + results.rounds[round].number + "...Fight!");
    console.log("Card for " + results.player1.name + ": " + results.rounds[round].player1Card.toString());
    console.log("Card for " + results.player2.name + ": " + results.rounds[round].player2Card.toString());

    if (results.rounds[round].winner == null) {
        console.log("Tie!");
    } else {
        console.log("Result: " + results.rounds[round].winner.name + " wins " + results.rounds[round].pointsAwarded + " point" + ((results.rounds[round].pointsAwarded != 1) ? "s" : ""));

        if (results.rounds[round].bonusPointsAwarded != 0) {
            console.log("BONUS! Additional " + results.rounds[round].bonusPointsAwarded + " point" + ((results.rounds[round].bonusPointsAwarded != 1) ? "s" : "") + " awarded");
        }
    }
}

console.log("\n=========================");
console.log("F I N A L   R E S U L T S");
console.log("=========================\n");
console.log(results.player1.name + ": " + results.player1Points + " point" + ((results.player1Points != 1) ? "s" : ""));
console.log(results.player2.name + ": " + results.player2Points + " point" + ((results.player2Points != 1) ? "s" : ""));

if (results.player1Points > results.player2Points) {
    console.log(results.player1.name + " defeats " + results.player2.name);
} else if (results.player1Points < results.player2Points) {
    console.log(results.player2.name + " defeats " + results.player1.name);
} else {
    console.log("Tie game, everyone wins. And loses.");
}